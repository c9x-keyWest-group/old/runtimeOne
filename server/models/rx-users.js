import { createRxDatabase, RxDatabase, addRxPlugin } from "rxdb";
import { checkAdapter } from 'rxdb';
addRxPlugin(require('pouchdb-adapter-localstorage'));
const userSchema = {
    title: "User Schema"
    version: 0,
    description: "Describes user",
    type: "object",
    "properties": {
        "name": {
            "type": "string",
            "primary": true
        }
    }

};


// create, read, update, 