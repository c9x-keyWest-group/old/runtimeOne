// create database (couchdb offline-first)

import { createRxDatabase } from "rxdb";
import { checkAdapter } from "rxdb";
import { create } from "rxdb/dist/types/plugins/key-compression";

const db = await createRxDatabase({ 
    name: "users-test-db",
    adapter: "couchdb",
    password: "somesecretpasswd",
    multiInstance: true,
    eventReduce
});

db.$.subcribe(changeEvent => console.dir(changeEvent));

// extract this to another module ?



