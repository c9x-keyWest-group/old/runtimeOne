// users.js

import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;
    @Column
    name: string;
    @Column
    userGroup: string;
    @Column
}


export class UserGroup {
    id: number;
    name: string;
    description: string;
}
import "reflect-metadata"
import { createConnection } from "typeorm";

createConnection({
    type:
        host: "localhost",
    port: 3306,
    username: "developer10",
    password: "hashedPasswd",
    database: "test",

})