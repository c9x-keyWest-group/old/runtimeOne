
'use strict'

const cluster = require('cluster');
const runtimeEventEmitter = require(./events);

const winston = require('winston');
const logger = winston.createLogger({
	level: 'info',
	format: winston.format.json(),
	defaultMeta: {},
	transports: [

		new winston.transports.File({ filename: 'error.log', level: 'error' }),
		new winston.transports.File({
			filename: 'combined.log },
	level: 'info'),
		new winston.transports.Console()
	],
	format: winston.format.combine(
		winston.format.colorize({ all: true }),
		winston.format.simple()
	)

1
)

// Querying Logs
/*
winston supports querying of logs with Loggly-like options. See Loggly Search API. Specifically: File, Couchdb, Redis, Loggly, Nssocket, and Http.

*/
const options = {
	from: new Date() - (24 * 60 * 60 * 1000),
	until: new Date(),
	limit: 10,
	start: 0,
	order: 'desc',
	fields: ['message']
};

//
// Find items logged between today and yesterday.
//
logger.query(options, function (err, results) {
	if (err) {
		/* TODO: handle me */
		throw err;
	}

	console.log(results);
});