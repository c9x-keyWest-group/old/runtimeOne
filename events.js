'use strict';
// events.js
//


const EventEmitter = require('events');
console.log(EventEmitter);

// const runtimeEmitter = new Eventemitter();


runtimeEmitter.once();

class runtimeEmitter extends EventEmitter {};

const runtimeEventsEmitter = new runtimeEmitter();
runtimeEventsEmitter.on('event', () => {
    console.log('runtimeone-runtimeEvent event occured.')
});
runtimeEventsEmitter.emit('event');
runtimeEventsEmitter.once('someNewListener', (event, listener) => {
    if (event === 'event') {
        if (!runtimeEventsEmitter.listenerCount(runtimeEmitter, 'event')) {
            console.log('what is going on here?')
        }
    }
 })
 const myEmitter = new MyEmitter();
 myEmitter.on('event', () => {});
 myEmitter.on('event', () => {});
 console.log(EventEmitter.listenerCount(myEmitter, 'event'));
 // Prints: 2
 
